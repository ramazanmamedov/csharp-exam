﻿using System.IO;
using System.Threading.Tasks;
using Exam12.Models;
using Exam12.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Exam12.Controllers
{
    public class GalleryController : Controller
    {
        private readonly ApplicationContext _applicationContext;
        private readonly UserManager<User> _userManager;

        public GalleryController(ApplicationContext applicationContext, UserManager<User> userManager)
        {
            _applicationContext = applicationContext;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(AddGalleryImageViewModel model)
        {
            var user = _userManager.GetUserAsync(User);
            using var stream = new FileStream(model.GetFilePath(), FileMode.Create);
            model.Image.CopyTo(stream);
            var galleryImage = new GalleryImage
            {
                ImagePath = model.GetImagePath(),
                PlaceId = model.PlaceId,
                UserId = user.Result.Id
            };
            await _applicationContext.AddAsync(galleryImage);
            await _applicationContext.SaveChangesAsync();
            return RedirectToAction("Index", "Place", new{placeId = model.PlaceId});
        }
    }
}