﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Exam12.Models;
using Exam12.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Exam12.Controllers
{
    public class PlaceController : Controller
    {
        private readonly ApplicationContext _applicationContext;

        public PlaceController(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        [HttpGet]
        public IActionResult Index(string placeId)
        {
            return View(_applicationContext.Places.Find(placeId));
        }


        [HttpGet]
        [Authorize]
        public IActionResult AddPlace() => View();

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPlace(AddPlaceViewModel model)
        {
            if (!ModelState.IsValid) return View();
            using var stream = new FileStream(model.GetFilePath(), FileMode.Create);
            model.MainImage.CopyTo(stream);
            
            var place = model.GetPlace();
            place.MainImagePath = model.GetAvatarImagePath();

            await _applicationContext.AddAsync(place);
            await _applicationContext.SaveChangesAsync();
            return RedirectToAction("Index", "Home");   
        }
        
        public IActionResult CheckTitle(AddPlaceViewModel model)
        {
            model.Title = model.Title.Trim(); 
            return Ok(!_applicationContext.Places.Any(p => 
                string.Equals(p.Title, model.Title, StringComparison.CurrentCultureIgnoreCase)));
        
        }
        
        public IActionResult LiveSearchAjaxResult(string keyWord)
        {
            if (!string.IsNullOrEmpty(keyWord))
            {
                keyWord = keyWord.ToLower().Trim();
            }

            var places = _applicationContext.Places.Where(u => u.Description.ToLower().Contains(keyWord)
                                                      || u.Title.ToLower().Contains(keyWord)).ToList();
            return PartialView("PartialViews/PlaceLiveSearchResult", places);
        }
    }
}