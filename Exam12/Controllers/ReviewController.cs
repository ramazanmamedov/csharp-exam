﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Exam12.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Exam12.Controllers
{
    public class ReviewController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly ApplicationContext _applicationContext;

        public ReviewController(ApplicationContext applicationContext, UserManager<User> userManager)
        {
            _applicationContext = applicationContext;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Add(string placeId, string message, double rating)
        {
            var user = _userManager.GetUserAsync(User);
            var review = new Review()
            {
                Message = message,
                DateTime = DateTime.Now,
                PlaceId = placeId,
                UserId = user.Result.Id,
                Rating = rating
            };
            await _applicationContext.Reviews.AddAsync(review);
            await _applicationContext.SaveChangesAsync();
            return Ok();
        }

        public IActionResult GiveReviews(string placeId )
        {
            var reviews = _applicationContext.Reviews.Where(r => r.PlaceId == placeId);
            return Json(reviews);
        }
        
        
    }
}