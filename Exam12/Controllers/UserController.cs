﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Exam12.Models;
using Exam12.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace Exam12.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UserController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Registration() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registration(RegistrationViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            using var stream = new FileStream(model.GetFilePath(), FileMode.Create);
            model.Avatar.CopyTo(stream);

            var user = model.GetUser();
            user.AvatarImagePath = model.GetAvatarImagePath();

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                return RedirectToAction("Index", "Home");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(model);
        }

        [HttpGet]
        
        public IActionResult LogIn(string returnUrl = null)
        {
            return View(new LogInViewModel {ReturnUrl = returnUrl});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogIn(LogInViewModel model)
        {
            var result = SignInResult.Failed;
            if (!ModelState.IsValid) return View(model);
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user != null)
            {
                result = await _signInManager.PasswordSignInAsync(user,
                    model.Password, model.RememberMe, false);
            }

            if (result.Succeeded)
            {
                if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                {
                    return Redirect(model.ReturnUrl);
                }

                return RedirectToAction("Index", "Home");
            }

            ModelState.AddModelError("", "Неправильный логин и (или) пароль");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
        
        public IActionResult CheckName(RegistrationViewModel model)
        {
            var user = model.GetUser();
            model.UserName = model.UserName.Trim();
            bool isTrue;
            if (string.IsNullOrEmpty(user.Id))
                isTrue = _userManager.Users.Any(b => string.Equals(b.UserName.Trim(), model.UserName,
                    StringComparison.CurrentCultureIgnoreCase));
            else
            {
                if (_userManager.Users.Any(b => string.Equals(b.UserName.Trim(), model.UserName,
                                                    StringComparison.CurrentCultureIgnoreCase) && b.Id == user.Id))
                {
                    return Ok(true);
                }

                return Ok(!_userManager.Users.Any(b => string.Equals(b.UserName.Trim(), model.UserName,
                    StringComparison.CurrentCultureIgnoreCase)));
            }
            return Ok(isTrue);
        }
        
        public IActionResult CheckEmail(RegistrationViewModel model)
        {
            model.Email = model.Email.Trim();
            return Ok(!_userManager.Users.Any(u => u.Email == model.Email));
        }
    }
}