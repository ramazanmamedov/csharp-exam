﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Exam12.Models
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<Place> Places { get; set; }
        public DbSet<GalleryImage> GalleryImages { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public ApplicationContext(DbContextOptions options) : base(options)
        {
            
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }
        
        
    }
    
   
}