﻿namespace Exam12.Models
{
    public class GalleryImage
    {
        public string Id { get; set; }
        public string PlaceId { get; set; }
        public virtual Place Place { get; set; }
        public string ImagePath { get; set; }
        public string UserId { get; set; }
        public virtual User User { get; set; }
       
    }
}