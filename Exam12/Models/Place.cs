﻿namespace Exam12.Models
{
    public class Place
    {
        public string Id { get; set; }
        public string MainImagePath { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Rating { get; set; }
    }
}