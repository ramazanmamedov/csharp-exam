﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Exam12.Models
{
    public class Review
    {
        public string Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Message { get; set; }
        public double Rating { get; set; }
        public string PlaceId { get; set; }
        public virtual Place Place { get; set; }
        public string UserId { get; set; }
        public virtual User User { get; set; }

        public Review(string placeId)
        {
            PlaceId = placeId;
        }

        public Review()
        {
        }
    }
}