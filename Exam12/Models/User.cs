﻿using Microsoft.AspNetCore.Identity;

namespace Exam12.Models
{
    public class User : IdentityUser
    {
        public string AvatarImagePath { get; set; }
    }
}