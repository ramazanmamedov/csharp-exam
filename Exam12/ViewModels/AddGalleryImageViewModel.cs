﻿using System.ComponentModel.DataAnnotations;
using System.IO;
using Exam12.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exam12.ViewModels
{
    public class AddGalleryImageViewModel
    {
        [Required]
        public IFormFile Image { get; set; }

        public string PlaceId { get; set; }

        public AddGalleryImageViewModel(string placeId)
        {
            PlaceId = placeId;
        }

        public AddGalleryImageViewModel()
        {
        }

        public string GetFilePath()
        {
            return Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot",
                "images",
                Image.FileName
            );
        }

        public string GetImagePath()
        {
            return Path.Combine(
                "images",
                Image.FileName
            );
        }
    }
}