﻿using System.ComponentModel.DataAnnotations;
using System.IO;
using Exam12.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exam12.ViewModels
{
    public class AddPlaceViewModel
    {
        [Required]
        [Remote("CheckTitle", "Place", ErrorMessage = "This Place Title is busy", AdditionalFields = "Id")]
        public string Title { get; set; }

        [Required]
        [MinLength(10)]
        public string Description { get; set; }

        [Required] 
        public IFormFile MainImage { get; set; }

        public Place GetPlace()
        {
            var place = new Place
            {
                Description = Description,
                Title = Title
            };

            return place;
        }

        public string GetFilePath()
        {
            return Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot",
                "images",
                MainImage.FileName
            );
        }

        public string GetAvatarImagePath()
        {
            return Path.Combine(
                "images",
                MainImage.FileName
            );
        }
    }
}