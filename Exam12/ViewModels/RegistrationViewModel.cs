﻿using System.ComponentModel.DataAnnotations;
using System.IO;
using Exam12.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exam12.ViewModels
{
    public class RegistrationViewModel
    {
        [Required]
        [Remote("CheckName", "User", ErrorMessage = "This User Name is busy", AdditionalFields = "Id")]
        public string UserName { get; set; }
        [Required]
        [Remote("CheckEmail", "User", ErrorMessage = "This Email is busy", AdditionalFields = "Id")]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public IFormFile Avatar { get; set; }
        
        public User GetUser()
        {
            var user = new User
            {
                Email = Email,
                UserName = UserName
            };

            return user;
        }
        
        public string GetFilePath()
        {
            return Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot",
                "images",
                Avatar.FileName
            );
        }

        public string GetAvatarImagePath()
        {
            return Path.Combine(
                "images",
                Avatar.FileName
            );
        }
    }
}